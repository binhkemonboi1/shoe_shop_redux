import logo from './logo.svg';
import './App.css';
import Ex_Shoe from './Shop_shop_redux/Ex_Shoe';

function App() {
  return (
    <div className="App">
      <Ex_Shoe />
    </div>
  );
}

export default App;
