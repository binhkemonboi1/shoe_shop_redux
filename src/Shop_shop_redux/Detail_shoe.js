import React, { Component } from 'react'

export default class Detail_shoe extends Component {
    render() {
        let { detail } = this.props;
        return (
            <div>
                <img width={200} src={detail.image} />
            </div>
        );
    }
}

