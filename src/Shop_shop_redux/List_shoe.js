import React, { Component } from 'react'
import Item_shoe from './Item_shoe';

export default class List_shoe extends Component {
    renderList = () => {
        return this.props.list.map((item, index) => {
            return <Item_shoe
                handleViewDetail={this.props.handleViewDetail}
                handleAddToCart={this.props.handleBuy}
                item={item} key={index} />;
        });
    };

    render() {
        return (
            <div className="row">
                {this.renderList()}
            </div>
        )
    }
}
